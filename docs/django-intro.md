# Introduction to Django

Content:

1. How to install Django
2. Create first Django app
3. Django project structure
4. Main components of Django app
5. Views
6. Models
7. Additional topics

## Install Django


### Install the latest version

```console
$ pip install Django
```

### Install specific version. 

Following command will install Django version `3.0.1`

```console
$ pip install Django==3.0.1
```

> **Tip**
> Use `virtual environments` for Django projects.
>
> * what is `virtual environemnt` (https://docs.python.org/3/library/venv.html)?
> * how to create `virtual environments`
>   (https://packaging.python.org/tutorials/installing-packages/#creating-virtual-environments)
> * how to use virtualenv?

### Verify Django installation

1. Print version info:

    ```console
    $ python -c "import django; print(django.get_version())"

    3.0.5
    ```
    or

    ```console
    $ python -m django --version

    3.0.5
    ```
2. Check if `django-admin` command is available

    ```console
    $ django-admin

    Type 'django-admin help <subcommand>' for help on a specific subcommand.

    Available subcommands:

    [django]
        check
        compilemessages
        createcachetable
        dbshell
        diffsettings
        dumpdata
        flush
        inspectdb
        loaddata
        makemessages
        makemigrations
        migrate
        runserver
        sendtestemail
        shell
        showmigrations
        sqlflush
        sqlmigrate
        sqlsequencereset
        squashmigrations
        startapp
        startproject
        test
        testserver
    ```

## Creating Django Project

_Project_ is a Python package that contains all the settings for an instance of
Django. This would include database configuration, Django-specific options and
application-specific settings.


### MyTodoList web app

Simple task manager and todo lists manager.


### Initialize Django project

Django provides command `django-admin` for various administrative tasks, one of
which is to create scafolding of a new Django project.

```bash
$ django-admin startproject mytodolist
```

This will create new folder `mytodolist` with following content:

```console
mytodolist/
├── manage.py
└── mytodolist
    ├── __init__.py
    ├── settings.py
    ├── urls.py
    └── wsgi.py
```

Let's see what was generated for us. Execute following command in terminal:

```console
$ cd mytodolist
$ python manage.py runserver
```


### `manage.py` - Command line app management

`manage.py` script provides number of administrative commands for managing
django project. You can see list of all available subcommands by executing:

```console
$ python manage.py
```

> **note**: `manage.py` vs `django-admin`
>
> `manage.py` provides all functionalities as django-admin but also takes care
> of a few project specific things:
>
> * adds your project's path on **sys.path**
> * sets **DJANGO_SETTINGS_MODULE** environment variable so that it points to
>   your project's **settings.py** file

Subset of the most frequently used subcommands:

* `runserver` - start development server
* `makemigrations` - create data migrations files. 
* `migrate` - apply migrations to database
* `startapp` - add new app to the project
* `createsuperuser` - create admin user for django project.


### Django apps

Let's add new application to `mytodolist` Django project.

```console
$ python manage.py startapp tasks
```

And here is how our project's folder structure looks like now:

```console
mytodolist/
├── manage.py
├── mytodolist
│   ├── __init__.py
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
└── tasks
    ├── __init__.py
    ├── admin.py
    ├── apps.py
    ├── migrations
    │   └── __init__.py
    ├── models.py
    ├── tests.py
    └── views.py
```

> **note**: Django Project vs Django App
>
> Django project is built from one or more of Django apps. Project combines one
> or more Django apps into one *web application*. 
> Project defines:
>
> * execution environment settings for all apps (database, security, logging, etc.),
> * top level URL routing,
> * WSGI application

### Views

#### "Hello Tasks" View

Let's implement index view for tasks app by adding following code to `tasks/views.py`:

```python
from django.http import HttpResponse


def index(request):
    return HttpResponse("Hello world! This is tasks app index site.")
```
The code above defines the simplest view possible in Django framework.

Next step is to bind index view to some URL. The simplest way to do this is to
add an entry to `mytodolist/urls.py`:

```python
from django.urls import path

from tasks import views

urlpatterns = [
    path('', views.index, name='index'),
]

```

The code above binds index view from tasks app to the root URL of the django
project.

> Notes:
>
  * URL structure, relative/absolute URLs, query params...
  * How to interpret `''` path? 
> * Why do we need `name` argument?

> *Todo*:
>
> * Let's correct this and bind tasks.views.index to
>   http://localhost:8000/tasks
> * Also make sure that both `/tasks` and `/tasks/` URLs are handled correctly.

### URL routing

[Docs](https://docs.djangoproject.com/en/3.0/topics/http/urls/)

Django provides all necessary tools for building flexible URL routing schemes.
The best practice is to define URL routing for each app and than include all
app routing rules into project URL routing rules.

1. Create urls.py module in tasks app (`tasks/urls.py`) and add following code:

```Python
from django.urls import path
from tasks import views
urlpatterns = [
    path('', views.index, name='index')
]
```


2. Include tasks app urlpatterns in project URL routing scheme by adding
   following code to `mytodolist/urls.py`:

```python
from django.urls import path, include
from django.contrib import admin

urlpatterns = [
    path('admin/', admin.site.urls),
    path('tasks/', include('tasks.urls')),
]

```

> **path() function**
>
> * (https://docs.djangoproject.com/en/3.0/ref/urls/#django.urls.path)
> 
> * The `path(route, view, kwargs=None, name=None)` function binds a view to an
>   URL pattern. i
>   * The route argument should be a string that contains a URL pattern. The
>     string may contain angle brackets (like <username> above) to capture part
>     of the URL and send it as a keyword argument to the view. The angle
>     brackets may include a converter specification (like the int part of
>     <int:section>) which limits the characters matched and may also change the type of the variable
>     passed to the view. For example, <int:section> matches a string of
>     decimal digits and converts the value to an int. See [How Django processes
>     a request for more
>     details](https://docs.djangoproject.com/en/3.0/topics/http/urls/#how-django-processes-a-request).
>
>   * The view argument is a view function or the result of as_view() for
>     class-based views. It can also be an django.urls.include().
>
>   * The kwargs argument allows you to pass additional arguments to the view
>     function or method. See [Passing extra options to view
>     functions](https://docs.djangoproject.com/en/3.0/topics/http/urls/#views-extra-options)
>     for an example.
>
>   * See [Naming URL
>     patterns](https://docs.djangoproject.com/en/3.0/topics/http/urls/#naming-url-patterns)
>     for why the name argument is useful.


> **include() function**
>
> * Docs: https://docs.djangoproject.com/en/3.0/ref/urls/#django.urls.include
>
> * The include() function allows referencing other URLconfs. Whenever Django
>   encounters include(), it chops off whatever part of the URL matched up to
>   that point and sends the remaining string to the included URLconf for
>   further processing.  
>
>   The idea behind include() is to make it easy to plug-and-play URLs. Since
>   tasks are in their own URLconf (tasks/urls.py), they can be placed under
>   “/tasks/”, or under “/mytasks/”, or under “/content/tasks/”, or any
>   other path root, and the app will still work.


-------------------------------------------------------------------------------

# Writing views

## View functions

A view function, or view for short, is simply a Python function that takes a
Web request and returns a Web response. This response can be the HTML contents
of a Web page, or a redirect, or a 404 error, or an XML document, or an
image... or anything, really.
[Docs](https://docs.djangoproject.com/en/3.0/topics/http/views/)

```Python
# tasks/views.py
from django.http import HttpResponse, JsonResponse


def index(request):
    return HttpResponse(
        '<html><body>Tasks App index view</body></html>')


def index_json(request):
    return JsonResponse({'msg': 'Tasks App index view'}, status=200)

```

```Python
# tasks/urls.py

from django.urls import path
from tasks import views

urlpatterns = [
    path('', views.index, name='index'),
    path('json/', views.index_json, name='index_json'),
]
```

> Notes: 
> 1. Which HTTP methods are handled in previous example?
> 2. How to handle different HTTP methods in a single view?
> 3. General discussion about HTTP methods... 


## Class-based views

Docs: (https://docs.djangoproject.com/en/3.0/topics/class-based-views/)

A view is a callable which takes a request and returns a response.

Class-based views provide an alternative way to implement views as Python
objects instead of functions. They do not replace function-based views, but
have certain differences and advantages when compared to function-based views:

- Organization of code related to specific HTTP methods (GET, POST, etc.) can be
addressed by separate methods instead of conditional branching.
- Object oriented techniques such as mixins (multiple inheritance) can be used to
factor code into reusable components.

### Class-based view example

```Python
#  tasks/views.py

from django.http import HttpResponse
from django.views import View


class IndexView(View):
    
    def get(self, request, *args, **kwargs):
        return HttpResponse('<html><body>Tasks App index view</body></html>')

    # def head(self, request, *args, **kwargs):
    #     pass
    
    # def post(self, request, *args, **kwargs):
    #     pass
    
    # def put(self, request, *args, **kwargs):
    #     pass 
    
    # def delete(self, request, *args, **kwargs):
    #     pass
```

> Notes:
>
> * explicit mapping between HTTP methods and methods.

### Update URL resolver

Because Django’s URL resolver expects to send the request and associated
arguments to a callable function, not a class, class-based views have an
`as_view()` class method which returns a function that can be called when a
request arrives for a URL matching the associated pattern.

```Python
from django.urls import path
from tasks import views
urlpatterns = [
    path('', views.IndexView.as_view(), name='index')
]
```

> Notes: 
>
> * You don't want to create HTML strings in python code.

## Templates

[Docs](https://docs.djangoproject.com/en/3.0/topics/templates/)

Being a web framework, Django needs a convenient way to generate HTML
dynamically. The most common approach relies on templates. A template contains
the static parts of the desired HTML output as well as some special syntax
describing how dynamic content will be inserted.

By default django will look up for templates files in `templates` directories
located in root of all registered django apps (apps listed in INSTALLED_APPS
list).

### Create template for our Tasks App index view

```html
{# tasks/index.html #}

{% load static %}

<!DOCTYPE html>
<html lang="en"> 
<head></head>
<body>
    Task App index view.
</body>    
</html>
```

and update IndexView implementation

```Python
# tasks/views.py

from django.shortcuts import render
from django.views import View

class IndexView(View):

    def get(self, request, *args, **kwargs):
        return render(request, 'tasks/index.html')
```

> Notes:
>
> * `render` shortcut function: Combines a given template with a given context
>   dictionary and returns an HttpResponse object with that rendered text.  
>   Docs: https://docs.djangoproject.com/en/3.0/topics/http/shortcuts/#render


### Let's add some dynamic content to index view

```html
{# tasks/index.html #}

{% load static %}

<!DOCTYPE html>
<html lang="en"> 
<head></head>
<body>
    <h1>Tasks App index view.</h1>
    <p>Current time: {{current_time}}</p>
</body>    
</html>
```

and update IndexView implementation

```Python
# tasks/views.py

from django.shortcuts import render
from django.utils.timezone import now
from django.views import View

class IndexView(View):

    def get(self, request, *args, **kwargs):
        context = {
            'current_time': now()
        }
        return render(request, 'tasks/index.html', context=context)
```

### Django Template Language
[Docs](https://docs.djangoproject.com/en/3.0/ref/templates/language/)

#### Variables
Variables look like this: `{{ variable }}`. When the template engine encounters
a variable, it evaluates that variable and replaces it with the result.


#### Filters
Filters look like this: `{{ name|lower }}`. This displays the value of the `{{
name }}` variable after being filtered through the `lower` filter, which converts
text to lowercase. Use a pipe (|) to apply a filter.

Example:
- datetime formatting: use date and time filters to improve the template above. 

#### Tags

Tags look like this: `{%%}`. Tags are more complex than variables: Some create
text in the output, some control flow by performing loops or logic, and some
load external information into the template to be used by later variables.

Sample tags:
- for: {% for %} ...loop body... {% endfor %}
- if, elif, else: {% if %} ... {% elif %} ... {% else %} ... {% endif %}
- block, extends: support for template inheritance

### Let's add some static content to our templates

Add following snippet to `tasks/index.html` in order to include CSS styles
defined in main `app.css` file.

```html
{# tasks/index.html #}
{% load  i18n static %}
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="{% static 'tasks/app.css' %}">
</head>

...

```

### Templates Inheritance

The idea is that we can inherit common layout elements and other HTML snippets
in all templates in the project/app. Few examples are:
- links to shared css/js files
- HTML for headers, footers and side-bar navigation
- ...

1. Create `base.html` file in `tasks/templates` folder.

```html
{# tasks/base.html #}
{% load  i18n static %}
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="{% static 'tasks/app.css' %}">
    <title>{%block title %}Tasks{% endblock %}</title>
</head>

<body>
    {% block page_content %}
    {% endblock %}
</body>

</html>
```
> Notes:
> - `block` tag

2. Update index.html

```html
{# tasks/index.html #}
{% extends 'tasks/base.html' %}

{% block page_content %}
<h1>Tasks App index view.</h1>
<p>
    Current time is {{current_time|time:"H:i:s"}}.
</p>
<p>
    Current date is {{current_time|date:"d.m.Y."}}.
</p>
{% endblock %}
```
> Notes:
>
> * `extends` tag
> * `block` tag
> * now we can change page layout in `base.html` file and all inheriting
>   tamplates will have updated layout

> Todo:
>
> * can you update title element in index.html template?
> * add common navigation element for all pages?


### Serving static files

Websites generally need to serve additional files such as images, JavaScript,
or CSS. In Django, we refer to these files as “static files”. Django provides
`django.contrib.staticfiles` to help you manage them.

[Docs](https://docs.djangoproject.com/en/3.0/howto/static-files/)

#### Where to store static assets?

Django will automatically include all `static` folders in apps added to
`INSTALLED_APPS` property (defined in `settings.py` file).

The best practice is to:

* include project related assets into `static` folder of your project folder
* include app specific assets into `<APP_NAME>/static/<APP_NAME>/` subfolder,
  where <APP_NAME> is name of the django application

For example:
* `app.css` which contains CSS styles definitions for general web site layout
  should be placed in `<PROJECT_ROOT>/mytodolist/static/app.css`
* `app.css` specific only for `tasks` app should be placed in
  `<PROJECT_ROOT>/tasks/static/tasks/app.css`

> Notes:
>
> * Why do we need `../tasks/static/tasks/...` folder structure?



-------------------------------------------------------------------------------

# Models and databases

## Models

Let's define models for Tasks app:

- Task: model for storing task details.
- TaskList: model for grouping tasks.

```python

from django.db import models
from django.utils import timezone


class Task(models.Model):

    PRIORITY_CHOICES = [(1, 'Low'), (3, 'Medium'), (5, 'High')]

    title = models.CharField(max_length=160)
    description = models.TextField(null=True, blank=True)
    deadline = models.DateTimeField(null=True, blank=True)

    priority = models.IntegerField(
        null=True, blank=True, choices=PRIORITY_CHOICES)

    created_at = models.DateTimeField(editable=False, default=timezone.now)

    task_list = models.ForeignKey(
        'TaskList',
        on_delete=models.CASCADE,
        related_name='tasks',
        null=True,
        blank=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('created_at', )


class TaskList(models.Model):

    title = models.CharField(max_length=80, unique=True)

```


## Sync models and database schema

### 1. Create DB schema migration scripts
```shell
$ python manage.py makemigrations
Migrations for 'tasks':
  tasks/migrations/0001_initial.py
    - Create model Task
    - Create model TaskList
    - Add field task_list to task
```
### 2. Apply migration scripts 

```shell
$ python manage.py migrate
Operations to perform:
  Apply all migrations: admin, auth, contenttypes, sessions, tasks
Running migrations:
  Applying contenttypes.0001_initial... OK
  Applying auth.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying admin.0003_logentry_add_action_flag_choices... OK
  Applying contenttypes.0002_remove_content_type_name... OK
  Applying auth.0002_alter_permission_name_max_length... OK
  Applying auth.0003_alter_user_email_max_length... OK
  Applying auth.0004_alter_user_username_opts... OK
  Applying auth.0005_alter_user_last_login_null... OK
  Applying auth.0006_require_contenttypes_0002... OK
  Applying auth.0007_alter_validators_add_error_messages... OK
  Applying auth.0008_alter_user_username_max_length... OK
  Applying auth.0009_alter_user_last_name_max_length... OK
  Applying sessions.0001_initial... OK
  Applying tasks.0001_initial... OK
```

## Register the models with Django admin app

Edit tasks/admin.py
```python
from django.contrib import admin
from tasks import models


@admin.register(models.Task)
class TaskAdmin(admin.ModelAdmin):
    pass


@admin.register(models.TaskList)
class TaskListAdmin(admin.ModelAdmin):
    pass
```

## The Django admin site

One of the most powerful parts of Django is the automatic admin interface. It
reads metadata from your models to provide a quick, model-centric interface
where trusted users can manage content on your site. The admin’s recommended
use is limited to an organization’s internal management tool. It’s not intended
for building your entire front end around.

ModelAdmin options:

- ModelAdmin.list_display
- ModelAdmin.list_filter
- ModelAdmin.list_per_page
- ModelAdmin.ordering
- ModelAdmin.search_field
- and many more options. See docs:
  (https://docs.djangoproject.com/en/3.0/ref/contrib/admin/#modeladmin-options)



> **TASKS:**
> 1. Create few tasks and task lists
> 2. Edit some of existing objects
> 3. Try to delete some objects

## Making Queries

### Creating objects

To represent database-table data in Python objects, Django uses an intuitive
system: A model class represents a database table, and an instance of that
class represents a particular record in the database table.

To create an object, instantiate it using keyword arguments to the model class,
then call `save()` to save it to the database.

```Python
from tasks.models import Task, TaskList

t = Task(title='My first task', priority=1)
t.save() 
```
### Updating objects

To update an object that’s already in the database, use `save()`.

Given a Task instance `t1` that has already been saved to the database, this
example changes its priority and updates its record in the database:

```Python
t1.priority = 5
t1.save()
```

### Retrieving objects

To retrieve objects from your database, construct a QuerySet via a Manager on
your model class.

A QuerySet represents a collection of objects from your database. It can have
zero, one or many filters. Filters narrow down the query results based on the
given parameters. In SQL terms, a QuerySet equates to a SELECT statement, and a
filter is a limiting clause such as WHERE or LIMIT.

You get a QuerySet by using your model’s Manager. Each model has at least one
Manager, and it’s called objects by default. Access it directly via the model
class, like so:

```Python
>>> from tasks.models import Task

>>> Task.objects
<django.db.models.manager.Manager object at ...>

```

#### Retrieving all objects

The simplest way to retrieve objects from a table is to get all of them. To do
this, use the all() method on a Manager: 

```Python
>>> Task.objects.all()
<QuerySet [<Task: task1>, ...]>
```

#### Retrieving specific objects with filters
The QuerySet returned by all() describes all objects in the database table.
Usually, though, you’ll need to select only a subset of the complete set of
objects.

To create such a subset, you refine the initial QuerySet, adding filter
conditions. The two most common ways to refine a QuerySet are:

**filter(\*\*kwargs)**

Returns a new QuerySet containing objects that match the given
lookup parameters.

**exclude(\*\*kwargs)** 

Returns a new QuerySet containing objects
that do not match the given lookup parameters.  

The lookup parameters (**kwargs
in the above function definitions) should be in the format described in Field
lookups below.

For example, to get a QuerySet of tasks with high priority (5), use filter()
like so:

```Python
>>> Task.objects.filter(priority=5)
```

> Notes
>
> - chaining filters
> - querysets are lazy
> - filtered querysets are uniqe


#### Retrieving a single object with get()

filter() will always give you a QuerySet, even if only a single object matches
the query - in this case, it will be a QuerySet containing a single element.

If you know there is only one object that matches your query, you can use the
get() method on a Manager which returns the object directly:

```Python
>>> task = Task.objects.get(pk=1)
```

You can use any query expression with get(), just like with filter() - again,
see Field lookups below.

Note that there is a difference between using get(), and using filter() with a
slice of [0]. If there are no results that match the query, get() will raise a
DoesNotExist exception. This exception is an attribute of the model class that
the query is being performed on - so in the code above, if there is no Entry
object with a primary key of 1, Django will raise Entry.DoesNotExist.

Similarly, Django will complain if more than one item matches the get() query.
In this case, it will raise MultipleObjectsReturned, which again is an
attribute of the model class itself.


### Field lookups

Field lookups are how you specify the meat of an SQL WHERE clause. They’re
specified as keyword arguments to the QuerySet methods filter(), exclude() and
get().

Basic lookups keyword arguments take the form field__lookuptype=value. (That’s
a double-underscore). For example:

```Python
>>> Task.objects.filter(priority__gte=2)

```

translates (roughly) into the following SQL:

```SQL
SELECT * FROM task WHERE priority >= 2;
```


The database API supports about two dozen lookup types; a complete reference
can be found in the field lookup reference
(https://docs.djangoproject.com/en/3.0/ref/models/querysets/#field-lookups).
To give you a taste of what’s available, here’s some of the more common lookups
you’ll probably use:

**exact**

```Python
>>> Task.objects.filter(title__exect='My first task')
```
If you don’t provide a lookup type – that is, if your keyword argument doesn’t
contain a double underscore – the lookup type is assumed to be exact.


**contains**, **icontains**, **startswith**, **endswith**

```Python
>>> Task.objects.filter(title__contains='buy')
>>> Task.objects.filter(title__endswith='car')
```



**lt**, **gt**, **lte**, **gte**, **range**
```Python
>>> Task.objects.filter(priority__lt=5)

>>> import datetime
>>> start_date = datetime.datetime(2019, 3, 1)
>>> end_date = datetime.datetime(2019, 4, 1)
>>> Task.objects.filter(created_at__gt=start_date, created_at__lt=end_date)
>>> Task.objects.filter(created_at__range=(start_date, end_date))
```

### Experimenting within Django Shell

Execute following command to start Django interactive shell

```bash
$ python manage.py shell
```




# Integrate models and views

## Define URL schema for `Tasks` app

```Python
urlpatterns = [
    path('', views.TaskListView.as_view(), name='task_list'),
    path('<int:task_id>/', views.TaskDetailsView.as_view(), name='task_details'),
    path('<int:task_id>/edit/', views.TaskEditView.as_view(), name='task_edit'),
    path('<int:task_id>/delete/', views.TaskDeleteView.as_view(), name='task_delete'),
]
```

> Notes:
>
> * Introducing `path parameters` - capturing values from urls: <int:task_id>
> * Path converters
>   [Docs](https://docs.djangoproject.com/en/3.0/topics/http/urls/#path-converters)
> * Using regular expressions - `re_path()'
>   [Docs](https://docs.djangoproject.com/en/3.0/topics/http/urls/#using-regular-expressions)

## Read-only views

```python
from django.views.generic import View
from django.contrib import messages
from tasks.models import Task


class TaskListView(View):
    """
    View for displaying list of tasks.
    """

    def get(self, request, *args, **kwargs):
        tasks = Task.objects.all()
        context = {
            'tasks': tasks
        }
        return render(request, 'tasks/task_list.html', context=context)


class TaskDetailsView(View):
    """
    View for displaying task details.
    """

    def get(self, request, task_id):
        context = {'task':Task.objects.get(id=task_id)}
        return render(request, 'tasks/task_details.html', context=context)

```

We need to update `task_list.html` and `task_details.html` templates

```html
{# tasks/task_list.html #}
{% extends 'base.html' %}

{% block page_content %}
<h1>Task List View</h1>
<div>
    <ul>
    {% for task in tasks%}
        <li><a href="{% url 'task_details' task_id=task.id %}">{{task.title}}</a></li>
    {% endfor %}
    </ul>
</div>
{% endblock %}

```

```html
{# tasks/task_details.html #}
{% extends 'tasks/base.html' %}

{% block page_content %}

{% if task %}
<div>
  Title: <strong>{{task.title}}</strong>
</div>
<div>
  Description: <strong>{{task.description}}</strong>
</div>
<div>
  Deadline: <strong>{{task.deadline}}</strong>
</div>
<div>
  Priority: <strong>{{task.get_priority_display}}</strong>
</div>

<div>
    <a href="{% url 'task_edit' task_id=task.id %}">Edit</a>
    <a href="{% url 'task_delete' task_id=task.id %}">Delete</a>
</div>
{% else %}

<div>
    Task not found... <a href="{% url 'task_list' %}">Back to tasks list</a>
</div>

{% endif %}


{% endblock %}
```


> Notes:
>
> * New template tags: `{% if %}`, `{% url %}`, {% for %}
> * How did we display `priority` attribute?



## Django Forms
[Docs](https://docs.djangoproject.com/en/3.0/topics/forms/)

In HTML, a form is a collection of elements inside `<form>...</form>` that
allow a visitor to do things like enter text, select options, manipulate
objects or controls, and so on, and then send that information back to the
server.

Django handles three distinct parts of the work involved in forms:

- preparing and restructuring data to make it ready for rendering
- creating HTML forms for the data
- receiving and processing submitted forms and data from the client


### The Form class

Add new file `forms.py` to tasks.

```Python
# forms.py

from django import forms
from tasks import models


class TaskForm(forms.Form):
    title = forms.CharField(label='Title', max_length=160)
    description = forms.CharField(
        label='Description', widget=forms.Textarea, required=False)
    deadline = forms.DateTimeField(label='Deadline', required=False)
    priority = forms.ChoiceField(
        label="Priority", choices=models.Task.PRIORITY_CHOICES, required=False)
```


### Update `TaskEditView`

We need to define handler for:

- GET method - this handler needs to create instance of TaskForm and add it to
  the context
- POST method - this handler needs to reconstruct instance of TaskForm from data
  sent in request, validate the data and update Task model or report error to
  user. 


```Python
# tasks/views.py


class TaskEditView(View):
    def get(self, request, task_id):
        try:
            task = Task.objects.get(id=task_id)
        except Task.DoesNotExist:
            raise Http404('Task does not exist')
        form = TaskForm(
            data={
                'title': task.title,
                'description': task.description,
                'deadline': task.deadline,
                'priority': task.priority
            })
        context = {'task': task, 'form': form}
        return render(request, 'tasks/task_edit.html', context=context)

```

### Rendering form in template

The simplest way to add form to `task_edit.html` template is:
```html
{# tasks/task_edit.html #}
{% extends 'tasks/base.html' %}

{% block title %}
tasks > edit
{% endblock%}

{% block page_content %}
<h1>Task edit view</h1>
<div>
    <form action="{% url 'task_edit' task_id=task.id %}" method="post">
        {% csrf_token %}
        {{ form }}
        <input type="submit" value="Save"/>
    </form>
</div>
{% endblock %}

```

The default form redering is not very sofisticated, it just renders form's 
`<label>` and `<input>` elements. There are few other options
for automatic rendering:

- `{{ form.as_table }}`
- `{{ form.as_p }}`
- `{{ form.as_ul }}`


If you need to create more flexibile layout with custom rendering of form
elements there is an option to render fields manually:

```html
{{ form.non_field_errors }}
<div class="fieldWrapper">
    {{ form.title.errors }}
    {{ form.title.label_tag }}
    {{ form.title }}
</div>
<div class="fieldWrapper">
    {{ form.link.errors }}
    {{ form.link.label_tag }}
    {{ form.link }}
</div>
...
```

### Processing submited form

Let's implement handler function which will read data submitted via TaskForm.

```Python
# tasks/views.py

class TaskEditView(View):
    def get(self, request, task_id):
        try:
            task = Task.objects.get(id=task_id)
        except Task.DoesNotExist:
            raise Http404('Task does not exist')
        form = TaskForm(
            data={
                'title': task.title,
                'description': task.description,
                'deadline': task.deadline,
                'priority': task.priority
            })
        context = {'task': task, 'form': form}
        return render(request, 'tasks/task_edit.html', context=context)

    def post(self, request, task_id):
        try:
            task = Task.objects.get(id=task_id)
        except Task.DoesNotExist:
            raise Http404('Task does not exist')
        form = TaskForm(request.POST)
        if form.is_valid():
            task.title = form.cleaned_data['title']
            task.description = form.cleaned_data['description']
            task.deadline = form.cleaned_data['deadline']
            task.priority = form.cleaned_data['priority']
            task.save()
            return HttpResponseRedirect(reverse('task_list'))
        context = {'task': task, 'form': form}
        return render(request, 'tasks/task_edit.html', context=context)

```

> Notes:
>
> * Data validation
> * Updating models from `clened_data`
> * Redirect on success
> * Render edit view on error.


### Model Forms
[Docs](https://docs.djangoproject.com/en/3.0/topics/forms/modelforms/)

Django provides helper class `ModelForm` that lets you create forms from Django
models.

```Python
# tasks/forms.py

from django import forms
from tasks import models

class TaskModelForm(forms.ModelForm):
    class Meta:
        model = models.Task
        fields = ['title', 'description', 'deadline', 'priority']

```


Update TaskEditView

```Python
class TaskEditView(View):

    def get(self, request, task_id):
        try:
            task = Task.objects.get(id=task_id)
        except Task.DoesNotExist:
            raise Http404('Task does not exist')
        form = TaskModelForm(instance=task)
        context = {'task': task, 'form': form}
        return render(request, 'tasks/task_edit.html', context=context)

    def post(self, request, task_id):
        try:
            task = Task.objects.get(id=task_id)
        except Task.DoesNotExist:
            raise Http404('Task does not exist')
        form = TaskModelForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('task_list'))
        context = {'task': task, 'form': form}
        return render(request, 'tasks/task_edit.html', context=context)
```

> Notes:
>
> * We did no make any changes in templates.
> * Automatic mapping from model fields to form fields.
> * Automatic validation rules.
> * Automatic widget selection based on model fields. (But can be overriden
>    with `widgets` property of `Meta` class) [Django
>    Widgets](https://docs.djangoproject.com/en/3.0/ref/forms/widgets/)

> Todo:
>
> * Change `priority` input widget to RadioSelect widget
> * Experiment with validation rules (e.g. min length of title)


### The Messages Framework

Quite commonly in web applications, you need to display a one-time notification
message (also known as “flash message”) to the user after processing a form or
some other types of user input.

For this, Django provides full support for cookie- and session-based messaging,
for both anonymous and authenticated users. The messages framework allows you
to temporarily store messages in one request and retrieve them for display in a
subsequent request (usually the next one). Every message is tagged with a
specific level that determines its priority (e.g., info, warning, or error).

Docs: (https://docs.djangoproject.com/en/3.0/ref/contrib/messages/)

#### Adding a message

```Python
# tasks/views.py

from django.shortcuts import render
from django.contrib import messages

from django.views import View
from tasks.models import Task


class TaskDetailsView(View):
    def get(self, request, task_id):
        context = {}
        try:
            task = Task.objects.get(id=task_id)
            context['task'] = task
        except Task.DoesNotExist:
            messages.error(request, 'Task does not exist')
        return render(request, 'tasks/task_details.html', context=context)

```



#### Displaying messages

Update `base.html` template:

```html
{# tasks/base.html #}
{% load  i18n static %}

<!DOCTYPE html>

<html lang="en">

<head>
    <link rel="stylesheet" href="{% static 'tasks/app.css' %}">
    <title>{%block title %}Tasks{% endblock %}</title>
</head>

<body>

    {% if messages %}
    <div class="messages">
    {% for message in messages %}
        <div class="callout {{ message.tags }}" data-closable>
            <p> {{ message }} </p>
        </div>
    {% endfor %}
    </div>
    {% endif %}
    
    {% block page_content %}
    {% endblock %}
</body>

```

### Tasks

1. Add cancel button task edit view
2. Implement task delete view
3. Implement "Add new task" 
4. List View - add pagination
5. List View - add search


-------------------------------------------------------------------------------

## Additional topics

### Django Settings
[Docs](https://docs.djangoproject.com/en/3.0/topics/settings/)

### Django management commands
[Docs](https://docs.djangoproject.com/en/3.0/howto/custom-management-commands/)

Applications can register their own actions with manage.py. For example, you
might want to add a manage.py action for a Django app that you’re
distributing.

### User Authentication in Django
[Docs](https://docs.djangoproject.com/en/3.0/topics/auth/)

The Django authentication system handles both authentication and authorization.
Briefly, authentication verifies a user is who they claim to be, and
authorization determines what an authenticated user is allowed to do. Here the
term authentication is used to refer to both tasks.

The auth system consists of:

- Users 
- Permissions: Binary (yes/no) flags designating whether a user may perform
a certain task. 
- Groups: A generic way of applying labels and permissions to
more than one user.  
- A configurable password hashing system 
- Forms and view
tools for logging in users, or restricting content 
- A pluggable backend system


### Deployment checklist
[Docs](https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/)
