from django import forms
from carsapp import models


class CarForm(forms.Form):
    title = forms.CharField(label='Title', max_length=255)
    link = forms.URLField(label='External link', max_length=255)
    company = forms.CharField(label='Company', max_length=255)
    year = forms.IntegerField(label='Year')
    engine_type = forms.ChoiceField(
        label="Engine Type", choices=models.Car.ENGINE_CHOICES)


class CarModelForm(forms.ModelForm):

    class Meta:
        model = models.Car
        fields = ['title', 'link', 'company', 'year', 'engine_type']