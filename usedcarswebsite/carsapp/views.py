from django.shortcuts import render

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.utils.timezone import now
from django.views import View
from django.urls import reverse

from carsapp.models import Car
from carsapp.forms import CarModelForm

class CarListView(View):

    def get(self, request, **kwargs):
        context = {
            'cars': Car.objects.all()
        }
        return render(request, 'carsapp/car_list.html', context=context)


class CarDetailsView(View):

    def get(self, request, car_id):
        context = {
            'car': Car.objects.get(id=car_id)
        }
        return render(request, 'carsapp/car_details.html', context=context)


class CarEditView(View):

    def get(self, request, car_id):
        car = Car.objects.get(id=car_id)
        
        form = CarModelForm(instance=car)
        
        context = {
            'form': form
        }
        return render(request, 'carsapp/car_edit.html', context=context)

    def post(self, request, car_id):
        car = Car.objects.get(id=car_id)
        car_form = CarModelForm(request.POST, instance=car)
        if car_form.is_valid():
            car_form.save()
            return HttpResponseRedirect(
                reverse('car_details', kwargs={'car_id': car_id}))
        
        context = {}
        context['form'] = car_form
        return render(request, 'carsapp/car_edit.html', context=context)


class CarAddView(View):

    def get(self, request):
        form = CarModelForm()
        context = {
            'form': form,
            'add_new': True
        }
        return render(request, 'carsapp/car_edit.html', context=context)

    def post(self, request):
        car_form = CarModelForm(request.POST)
        if car_form.is_valid():
            new_car = car_form.save()
            return HttpResponseRedirect(
                reverse('car_details', kwargs={'car_id': new_car.id}))

        context = {
            'form': car_form,
            'add_new': True
        }
        return render(request, 'carsapp/car_edit.html', context=context)


class CarDeleteView(View):
    def get(self, request, **kwargs):
        context = {}

        return render(request, 'carsapp/car_delete.html', context=context)

