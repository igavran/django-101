from django.urls import path
from carsapp import views

urlpatterns = [
    path('', views.CarListView.as_view(), name='car_list'),
    path('add/', views.CarAddView.as_view(), name='car_add'),
    path('<int:car_id>/', views.CarDetailsView.as_view(), name='car_details'),
    path('<int:car_id>/', views.CarDetailsView.as_view(), name='car_details'),
    path('<int:car_id>/edit/', views.CarEditView.as_view(), name='car_edit'),
    path('<int:car_id>/delete/', views.CarDeleteView.as_view(), name='car_delete'),
]
